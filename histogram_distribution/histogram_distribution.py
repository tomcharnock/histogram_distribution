__author__="Tom Charnock"
__version__="0.1"

import numpy as np
import sys

class HistogramDistribution():
    def __init__(self, samples, prior_ranges, bins=50):
        shape = samples.shape
        if len(shape) == 2:
            samples = samples[:, np.newaxis]
            shape = samples.shape
            self.squeeze = True
        else:
            self.squeeze = False
        self.n_samples = shape[0]
        self.n_distributions = shape[1]
        self.n_dims = shape[2]
        self.prior_ranges = prior_ranges
        if type(bins) == int:
            self.bins = tuple([bins for i in range(self.n_dims)])
        else:
            self.bins = tuple(bins)
        self.n_bins = np.prod(self.bins)
        self.error_float = 1e-300
        self.hists, self.edges = self.get_distributions(samples)
        self.domain_size = np.prod([edge[1] - edge[0] for edge in self.edges])
        if self.squeeze:
            self.hists /= (self.hists * self.domain_size).sum()
        else:
            self.hists /= np.expand_dims(
                (self.hists * self.domain_size).sum(
                    tuple(np.arange(1, self.n_dims+1))),
                tuple(np.arange(1, self.n_dims+1)))

    def get_distributions(self, samples):
        hists = np.zeros((self.n_distributions,) + self.bins)
        for i in range(self.n_distributions):
            hist, edges = np.histogramdd(
                samples[:, i],
                bins=self.bins,
                range=self.prior_ranges,
                density=True)
            hists[i] = hist
        hists = np.where(np.isnan(hists), np.ones_like(hists) * self.error_float, hists)
        if self.squeeze:
            hists = hists[0]
        return hists, edges

    def get_batch_shape(self, sample):
        shape = sample.shape
        if len(shape) == 1:
            batch_shape = (1,)
            sample = sample[np.newaxis]
            squeeze = True
        else:
            batch_shape = shape[:-1]
            squeeze = False
        return sample, batch_shape, squeeze

    def get_indices(self, sample, batch_shape=None):
        if batch_shape is None:
            sample, batch_shape = self.get_batch_shape(sample)
        if sample.shape[-1] != self.n_dims:
            print("Sample has wrong event shape")
            sys.exit()
        inds = np.repeat([
             np.searchsorted(
                 self.edges[i],
                 sample[..., i])[..., np.newaxis]-1
             for i in range(self.n_dims)],
            self.n_distributions, axis=-1)
        if self.squeeze:
            return inds
        else:
            hist_inds = np.tile(
                np.arange(self.n_distributions)[np.newaxis],
                np.prod(batch_shape)).reshape(
                    (1,) + batch_shape + (self.n_distributions,))
            return np.concatenate([hist_inds, inds], axis=0)

    def prob(self, sample):
        sample, batch_shape, squeeze = self.get_batch_shape(sample)
        if sample.shape[-1] != self.n_dims:
            print("Sample has wrong event shape")
            sys.exit()
        inds = self.get_indices(sample, batch_shape=batch_shape)
        inds = inds.reshape((inds.shape[0], -1))
        upper_limit = np.array(self.bins)
        if not self.squeeze:
            upper_limit = np.concatenate([
                np.array([self.n_distributions]),
                upper_limit])
        keep = np.all(
            np.logical_and(
                np.greater_equal(inds, 0),
                np.less(
                    inds,
                    upper_limit[:, np.newaxis])),
            axis=0)

        probs = np.ones(inds.shape[-1]) * np.nan
        probs[keep] = self.hists[tuple(inds[:, keep])]
        if squeeze:
            batch_shape = batch_shape[1:]
        if not self.squeeze:
            batch_shape = batch_shape + (self.n_distributions,)
        return probs.reshape(batch_shape)

    def log_prob(self, sample):
        return np.log(self.prob(sample, batch_shape=batch_shape))

    def sample(self, n=None):
        if n is None:
            n = 1
            squeeze = True
        else:
            squeeze = False
        if self.squeeze:
            self.hists = self.hists[np.newaxis]
        choices = np.array([
            np.unravel_index(
                np.random.choice(
                    np.arange(self.n_bins),
                    size=n,
                    p=self.hists[i].reshape((-1)) * self.domain_size),
            self.bins)
            for i in range(self.n_distributions)])
        lower = np.where(np.greater(choices-1, 0), choices-1, choices)
        upper = np.where(
            np.less(choices+1,
                    np.array(self.bins)[np.newaxis, :, np.newaxis]),
            choices+1,
            choices)
        samples = np.transpose(
            np.array([
                np.random.uniform(
                    self.edges[i][lower[:, i].reshape(-1)],
                    self.edges[i][upper[:, i].reshape(-1)]).reshape(
                        (self.n_distributions, n))
                for i in range(self.n_dims)]),
            (2, 1, 0))
        if self.squeeze:
            samples = samples[:, 0]
            self.hists = self.hists[0]
        if squeeze:
            samples = samples[0]
        return samples
