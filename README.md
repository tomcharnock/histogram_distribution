# Multi-distribution probability based on histograms and samples

This module provides a method to create a multivariate sampler and probability evaluation function based on histograms.

The *n*-d histograms are generated from a chain with shape
```python
chain.shape
> (n_samples, event_size)
```
or
```python
> (n_samples, n_distributions, event_size)
```
where `n_samples` is the number of samples in the chain, `n_distributions` is the number of different distributions the samples come from and `event_size` is the dimension of a single sample. For example if there are 10,000 samples from two different distributions with six parameters
```python
chain.shape
> (10000, 2, 6)
```
If there is only on distribution then the chain can be of either the form
```python
chain.shape
> (10000, 6)
```
or
```python
> (10000, 1, 6)
```
but note that the distribution axis will remain when sampling or calculating the probability.

```python
hd = HistogramDistribution(samples, prior_ranges, bins=50)
```
- `samples` - `np.ndarray` `(n_samples, event_size)]` or `(n_samples, n_distributions, event_size)`: the samples defining the histogram distribution(s).
- `prior_ranges` - `np.ndarray` `(event_size, 2)`: the lower and upper ranges of the prior distribution, any samples outside of this range will get appended to the edge (skewing the distribution). The larger the range the more bins will be necessary. Lower ranges are in `prior_ranges[:, 0]` and upper ranges are in `prior_ranges[:, 1]`.
- `bins` - `int` or `np.ndarray`, `list` or `tuple` `(event_size,)`: the number of bins (for all dimensions if using an `int`) or an `array`, `list` or `tuple` with the number of bins for each dimension.

```python
hd.prob(sample)
```
- `sample` - `np.ndarray` `(event_size,)`, `batch_shape + (event_size,)`: sample or samples at which to calculate the probability according to the histogrammed samples.

```python
hd.prob(sample)
```
- `sample` - `np.ndarray` `(event_size,)`, `batch_size + (event_size,)`: sample or samples at which to calculate the log probability according to the histogrammed samples.

```python
hd.sample(n=None)
```
- `n` - `None` or `int` - if no argument is passed, returns a sample drawn from the histogram with shape `(n_distributions, event_size)` or `(event_size,)` depending on whether several distributions are being considered at once. If an integer is passed then `sample(n)` returns an array of samples from the histogram with shape `(n, n_distributions, event_size)` or `(n, event_size)` depending on whether several distributions are being considered at once.

# To do
- Write comments
- Improve README
- Include examples
- Write tests
